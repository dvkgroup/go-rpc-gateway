package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/config"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/provider"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/router"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/server"
	internal "gitlab.com/dvkgroup/go-rpc-extension/infrastructure/service"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/tools/cryptography"
	"gitlab.com/dvkgroup/go-rpc-gateway/internal/endpoints"
	"gitlab.com/dvkgroup/go-rpc-gateway/internal/modules"
	"gitlab.com/dvkgroup/go-rpc-gateway/internal/storages"
	"gitlab.com/dvkgroup/go-rpc-gateway/internal/tbot"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server // not use
	Sig      chan os.Signal
	Storages *storages.Storages // not use
	Servises *modules.Services
	tbot     *tbot.TelegramBot
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запуск телеграмбота
	errGroup.Go(func() error {
		err := a.tbot.Run(context.Background())
		if err != nil {
			a.logger.Error("app: telegram bot error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)

	// инициализация сервисов
	services := modules.NewServices(nil, components)
	a.Servises = services

	controllers := modules.NewControllers(services, components)

	// инициализиция телеграмбота
	a.tbot = tbot.NewTelegramBot(a.conf.RabbitMQ, a.logger)

	// инициализция endpoints
	endpoint := endpoints.NewApiRouter(controllers, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(endpoint)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
