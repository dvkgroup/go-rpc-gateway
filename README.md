# Go rpc

Скачать репозитории
```bash
git clone https://gitlab.com/dvkgroup/go-rpc-gateway.git
git clone https://gitlab.com/dvkgroup/go-rpc-auth.git
git clone https://gitlab.com/dvkgroup/go-rpc-user.git
git clone https://gitlab.com/dvkgroup/go-rpc-exchange.git
```
Перейти в каталог gateway
```bash
cd go-rpc-gateway
```
Дать права на запуск для скриптов
```bash
chmod +x *.sh
```
Запуск
```bash
./launch.sh
```

Gateway находится на порту 8080 [link](http://localhost:8080/swagger)<br>
Auth на порту 8081 [link](http://localhost:8081/swagger)<br>
User на порту 8082 [link](http://localhost:8082/swagger)<br>
Exchange на порту 8083 [link](http://localhost:8083/swagger)

База auth (postgres) доступна на порту 5432, redis - 6379<br>
База user (postgres) доступна на порту 5433, redis - 6380<br>
База exch (postgres) доступна на порту 5434, redis - 6381

Консоль управления rabbitmq доступна по порту 15672 [link](http://localhost:15672)

[Telegram bot](https://t.me/r2d2_exchange_bot)

Остановка
```bash
./down.sh
```