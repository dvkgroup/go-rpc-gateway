package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/handlers"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service interfaces.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service interfaces.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), messages.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: messages.UserData{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, messages.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: messages.UserData{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	u.Profile(w, r)
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req messages.UserChangePasswordRequest
	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out := u.service.ChangePassword(r.Context(), messages.ChangePasswordIn{
		UserID:      claims.ID,
		OldPassword: req.OldPassword,
		NewPassword: req.NewPassword,
	})

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.UserChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Message:   "change password error",
		})
		return
	}

	u.OutputJSON(w, messages.UserChangePasswordResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Message:   "success change password",
	})
}
