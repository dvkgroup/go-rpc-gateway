package service

import (
	"context"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
)

type UserServiceJSONRPC struct {
	client interfaces.Userer
}

func NewUserRPCService(client interfaces.Userer) *UserServiceJSONRPC {
	u := &UserServiceJSONRPC{client: client}

	return u
}

func (t *UserServiceJSONRPC) Create(ctx context.Context, in messages.UserCreateIn) messages.UserCreateOut {
	return t.client.Create(ctx, in)
}

func (t *UserServiceJSONRPC) Update(ctx context.Context, in messages.UserUpdateIn) messages.UserUpdateOut {
	return t.client.Update(ctx, in)
}

func (t *UserServiceJSONRPC) VerifyEmail(ctx context.Context, in messages.UserVerifyEmailIn) messages.UserUpdateOut {
	return t.client.VerifyEmail(ctx, in)
}

func (t *UserServiceJSONRPC) ChangePassword(ctx context.Context, in messages.ChangePasswordIn) messages.ChangePasswordOut {
	return t.client.ChangePassword(ctx, in)
}

func (t *UserServiceJSONRPC) GetByEmail(ctx context.Context, in messages.GetByEmailIn) messages.UserOut {
	return t.client.GetByEmail(ctx, in)
}

func (t *UserServiceJSONRPC) GetByPhone(ctx context.Context, in messages.GetByPhoneIn) messages.UserOut {
	return t.client.GetByPhone(ctx, in)
}

func (t *UserServiceJSONRPC) GetByID(ctx context.Context, in messages.GetByIDIn) messages.UserOut {
	return t.client.GetByID(ctx, in)
}

func (t *UserServiceJSONRPC) GetByIDs(ctx context.Context, in messages.GetByIDsIn) messages.UsersOut {
	return t.client.GetByIDs(ctx, in)
}
