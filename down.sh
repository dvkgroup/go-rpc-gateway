#/bin/bash

docker compose down
cd ../go-rpc-auth
docker compose down
cd ../go-rpc-user
docker compose down
cd ../go-rpc-exchange
docker compose down

docker network rm external_app_network