package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/handlers"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"net/http"
	"net/mail"
)

type Auther interface {
	Register(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
	Verify(w http.ResponseWriter, r *http.Request)
}

type Auth struct {
	auth interfaces.Auther
	responder.Responder
	godecoder.Decoder
}

func NewAuth(service interfaces.Auther, components *component.Components) Auther {
	return &Auth{auth: service, Responder: components.Responder, Decoder: components.Decoder}
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	var req messages.AuthRegisterRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}

	if !valid(req.Email) {
		a.OutputJSON(w, messages.AuthRegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.AuthData{
				Message: "invalid email",
			},
		})
		return
	}

	if req.Password != req.RetypePassword {
		a.OutputJSON(w, messages.AuthRegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.AuthData{
				Message: "passwords mismatch",
			},
		})
		return
	}

	out := a.auth.Register(r.Context(), messages.RegisterIn{
		Email:    req.Email,
		Password: req.Password,
	})

	if out.ErrorCode != errors.NoError {
		msg := "register error"
		if out.ErrorCode == errors.UserServiceUserAlreadyExists {
			msg = "User already exists, please check your email"
		}
		a.OutputJSON(w, messages.AuthRegisterResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.AuthData{
				Message: msg,
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthRegisterResponse{
		Success: true,
		Data: messages.AuthData{
			Message: "verification link sent to " + req.Email,
		},
	})
}

func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	var req messages.AuthLoginRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.AuthLoginData{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.AuthorizeEmail(r.Context(), messages.AuthorizeEmailIn{
		Email:    req.Email,
		Password: req.Password,
	})
	if out.ErrorCode == errors.AuthServiceUserNotVerified {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.AuthLoginData{
				Message: "user email is not verified",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.AuthLoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthResponse{
		Success: true,
		Data: messages.AuthLoginData{
			Message:      "success login",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Refresh(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	out := a.auth.AuthorizeRefresh(r.Context(), messages.AuthorizeRefreshIn{UserID: claims.ID})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.AuthLoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthResponse{
		Success: true,
		Data: messages.AuthLoginData{
			Message:      "success refresh",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Verify(w http.ResponseWriter, r *http.Request) {
	var req messages.AuthVerifyRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, messages.VerifyResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.AuthData{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.VerifyEmail(r.Context(), messages.VerifyEmailIn{
		Email: req.Email,
		Hash:  req.Hash,
	})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.VerifyResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.AuthData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.VerifyResponse{
		Success: true,
		Data: messages.AuthData{
			Message: "email verification success",
		},
	})
}
