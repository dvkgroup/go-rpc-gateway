package modules

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	grpcauth "gitlab.com/dvkgroup/go-rpc-extension/transport/grpc/auth"
	grpcexch "gitlab.com/dvkgroup/go-rpc-extension/transport/grpc/exch"
	grpcuser "gitlab.com/dvkgroup/go-rpc-extension/transport/grpc/user"
	jrpcauth "gitlab.com/dvkgroup/go-rpc-extension/transport/jrpc/auth"
	jrpcexch "gitlab.com/dvkgroup/go-rpc-extension/transport/jrpc/exch"
	jrpcuser "gitlab.com/dvkgroup/go-rpc-extension/transport/jrpc/user"
	aservice "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/auth/service"
	eservice "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/exch/service"
	uservice "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/user/service"
	"gitlab.com/dvkgroup/go-rpc-gateway/internal/storages"
)

type Services struct {
	Auth interfaces.Auther
	User interfaces.Userer
	Exch interfaces.Exchanger
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	var ca interfaces.Auther
	var cu interfaces.Userer
	var ce interfaces.Exchanger

	switch components.Conf.RPCServer.Type {
	case "grpc":
		ca = grpcauth.NewGRPCClient(components.Conf.AuthRPC, components.Logger)
		cu = grpcuser.NewGRPCClient(components.Conf.UserRPC, components.Logger)
		ce = grpcexch.NewGRPCClient(components.Conf.ExchangeRPC, components.Logger)
	case "jsonrpc":
		ca = jrpcauth.NewAuthJsonRPCClient(components.Conf.AuthRPC, components.Logger)
		cu = jrpcuser.NewJSONRPCClient(components.Conf.UserRPC, components.Logger)
		ce = jrpcexch.NewJSONRPCClient(components.Conf.ExchangeRPC, components.Logger)
	}

	authClientRPC := aservice.NewAuthRPCService(ca)
	userClientRPC := uservice.NewUserRPCService(cu)
	exchClientRPC := eservice.NewExchRPCService(ce)

	return &Services{
		Auth: authClientRPC,
		User: userClientRPC,
		Exch: exchClientRPC,
	}
}
