package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/errors"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/responder"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
	"net/http"
)

type Exchanger interface {
	GetList(http.ResponseWriter, *http.Request)
	GetMinList(http.ResponseWriter, *http.Request)
	GetMaxList(http.ResponseWriter, *http.Request)
	GetAverageList(http.ResponseWriter, *http.Request)
	GetTicker(http.ResponseWriter, *http.Request)
}

type User struct {
	service interfaces.Exchanger
	responder.Responder
	godecoder.Decoder
}

func NewExch(service interfaces.Exchanger, components *component.Components) Exchanger {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) GetList(w http.ResponseWriter, r *http.Request) {
	out := u.service.GetList(r.Context())
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ExchangeListResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
		})
		return
	}

	u.OutputJSON(w, messages.ExchangeListResponse{
		Success: true,
		Data:    out.Data,
	})
}
func (u *User) GetMinList(w http.ResponseWriter, r *http.Request) {
	out := u.service.GetMinList(r.Context())
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ExchangeResponse{
			ErrorCode: out.ErrorCode,
		})
		return
	}

	u.OutputJSON(w, messages.ExchangeResponse{
		Success: true,
		Data:    out.Data,
	})
}

func (u *User) GetMaxList(w http.ResponseWriter, r *http.Request) {
	out := u.service.GetMaxList(r.Context())
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ExchangeResponse{
			ErrorCode: out.ErrorCode,
		})
		return
	}

	u.OutputJSON(w, messages.ExchangeResponse{
		Success: true,
		Data:    out.Data,
	})
}

func (u *User) GetAverageList(w http.ResponseWriter, r *http.Request) {
	out := u.service.GetAverageList(r.Context())
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ExchangeResponse{
			ErrorCode: out.ErrorCode,
		})
		return
	}

	u.OutputJSON(w, messages.ExchangeResponse{
		Success: true,
		Data:    out.Data,
	})
}

func (u *User) GetTicker(w http.ResponseWriter, r *http.Request) {
	out := u.service.GetTicker(r.Context())
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ExchangeResponse{
			ErrorCode: out.ErrorCode,
		})
		return
	}

	u.OutputJSON(w, messages.ExchangeResponse{
		Success: true,
		Data:    out.Data,
	})
}
