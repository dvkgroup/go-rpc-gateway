#/bin/bash

docker network create external_app_network

docker compose up -d
cd ../go-rpc-auth
docker compose up -d
cd ../go-rpc-user
docker compose up -d
cd ../go-rpc-exchange
docker compose up -d
