package modules

import (
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/component"
	acontroller "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/auth/controller"
	econtroller "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/exch/controller"
	ucontroller "gitlab.com/dvkgroup/go-rpc-gateway/internal/modules/user/controller"
)

type Controllers struct {
	Auth acontroller.Auther
	User ucontroller.Userer
	Exch econtroller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)
	userController := ucontroller.NewUser(services.User, components)
	exchController := econtroller.NewExch(services.Exch, components)

	return &Controllers{
		Auth: authController,
		User: userController,
		Exch: exchController,
	}
}
