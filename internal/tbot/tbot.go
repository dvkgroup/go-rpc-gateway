package tbot

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	amqpgo "github.com/rabbitmq/amqp091-go"
	"gitlab.com/dvkgroup/go-rpc-extension/config"
	"go.uber.org/zap"
	"sync"
)

type TelegramBot struct {
	conf    config.AMQPClient
	logger  *zap.Logger
	conn    *amqpgo.Connection
	ch      *amqpgo.Channel
	bot     *tgbotapi.BotAPI
	clients map[int64]struct{}
	sync.RWMutex
}

func NewTelegramBot(conf config.AMQPClient, logger *zap.Logger) *TelegramBot {
	bot, err := tgbotapi.NewBotAPI("6373774761:AAEkbJIySvOb6jj0jzOPF8RmqPzGwXNYDoI")
	if err != nil {
		return nil
	}
	clients := make(map[int64]struct{})

	return &TelegramBot{conf: conf, logger: logger, bot: bot, clients: clients}
}

func (b *TelegramBot) Run(ctx context.Context) error {

	go b.updateClients(ctx)

	return b.loadMessage(ctx)
}

func (b TelegramBot) updateClients(ctx context.Context) error {
	//b.bot.Debug = true
	//Устанавливаем время обновления
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 10

	//u.AllowedUpdates = []string{"message", "chat_member"}
	u.AllowedUpdates = []string{"message"}

	//Получаем обновления от бота
	updates := b.bot.GetUpdatesChan(u)

	var update tgbotapi.Update

	for {
		select {
		case <-ctx.Done():
			return nil

		case update = <-updates:
			if update.Message == nil {
				continue
			}

			b.Lock()
			b.clients[update.Message.Chat.ID] = struct{}{}
			b.Unlock()
		}
	}
}

func (b *TelegramBot) loadMessage(ctx context.Context) error {
	var err error

	connStr := fmt.Sprintf("amqp://%s:%s@%s:%s/", b.conf.User, b.conf.Password, b.conf.Host, b.conf.Port)

	b.conn, err = amqpgo.Dial(connStr)
	if err != nil {
		b.logger.Error("unable to open connect to RabbitMQ server. Error: %s", zap.Error(err))
		return err
	}

	defer func() {
		_ = b.conn.Close() // Закрываем подключение в случае удачной попытки подключения
	}()

	b.ch, err = b.conn.Channel()
	if err != nil {
		b.logger.Error("failed to open a channel. Error: %s", zap.Error(err))
		return err
	}

	defer func() {
		_ = b.ch.Close() // Закрываем подключение в случае удачной попытки подключения
	}()

	q, err := b.ch.QueueDeclare(
		"exchange", // name
		false,      // durable
		false,      // delete when unused
		false,      // exclusive
		false,      // no-wait
		nil,        // arguments
	)
	if err != nil {
		b.logger.Error("failed to declare a queue. Error: %s", zap.Error(err))
		return err
	}

	for {
		messages, err := b.ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		if err != nil {
			b.logger.Error("failed to register a consumer. Error: %s", zap.Error(err))
			return err
		}

		for message := range messages {
			b.RLock()
			for k := range b.clients {

				msg := tgbotapi.NewMessage(k, string(message.Body))
				_, err := b.bot.Send(msg)

				if err != nil {
					b.logger.Error("failed to send message.", zap.Error(err))
					delete(b.clients, k)
				}
			}
			b.RUnlock()
		}
	}
}
