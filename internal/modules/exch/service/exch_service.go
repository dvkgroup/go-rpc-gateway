package service

import (
	"context"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
)

type ExchServiceJSONRPC struct {
	client interfaces.Exchanger
}

func NewExchRPCService(client interfaces.Exchanger) *ExchServiceJSONRPC {
	u := &ExchServiceJSONRPC{client: client}

	return u
}

func (t *ExchServiceJSONRPC) GetList(ctx context.Context) messages.ExchangeListOut {
	return t.client.GetList(ctx)
}

func (t *ExchServiceJSONRPC) GetMinList(ctx context.Context) messages.ExchangeOut {
	return t.client.GetMinList(ctx)
}

func (t *ExchServiceJSONRPC) GetMaxList(ctx context.Context) messages.ExchangeOut {
	return t.client.GetMaxList(ctx)
}

func (t *ExchServiceJSONRPC) GetAverageList(ctx context.Context) messages.ExchangeOut {
	return t.client.GetAverageList(ctx)
}

func (t *ExchServiceJSONRPC) GetTicker(ctx context.Context) messages.ExchangeOut {
	return t.client.GetTicker(ctx)
}
