package storages

import (
	"gitlab.com/dvkgroup/go-rpc-extension/db/adapter"
	"gitlab.com/dvkgroup/go-rpc-extension/infrastructure/cache"
)

type Storages struct {
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{}
}
