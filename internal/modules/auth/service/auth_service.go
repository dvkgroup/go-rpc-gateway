package service

import (
	"context"
	"gitlab.com/dvkgroup/go-rpc-extension/interfaces"
	"gitlab.com/dvkgroup/go-rpc-extension/messages"
)

type AuthServiceJSONRPC struct {
	client interfaces.Auther
}

func NewAuthRPCService(client interfaces.Auther) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u
}

func (t *AuthServiceJSONRPC) Register(ctx context.Context, in messages.RegisterIn) messages.RegisterOut {
	return t.client.Register(ctx, in)
}

func (t *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in messages.AuthorizeEmailIn) messages.AuthorizeOut {
	return t.client.AuthorizeEmail(ctx, in)
}

func (t *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in messages.AuthorizeRefreshIn) messages.AuthorizeOut {
	return t.client.AuthorizeRefresh(ctx, in)
}

func (t *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in messages.AuthorizePhoneIn) messages.AuthorizeOut {
	return t.client.AuthorizePhone(ctx, in)
}

func (t *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in messages.SendPhoneCodeIn) messages.SendPhoneCodeOut {
	return t.client.SendPhoneCode(ctx, in)
}

func (t *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in messages.VerifyEmailIn) messages.VerifyEmailOut {
	return t.client.VerifyEmail(ctx, in)
}
